package pax.ips.com.pax_mpos.util;

/**
 * packer interface
 * 
 */
public interface IPacker {

    /**
     * get APDU interface
     * 
     * @return APDU interface
     */
    public IApdu getApdu();

}
