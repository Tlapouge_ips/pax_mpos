package pax.ips.com.pax_mpos.App2AppData.enums;

public enum TerminalIDstatus {
    TID_STATUS_NOT_INITIALIZED,  TID_STATUS_NOT_CONFIGURED,  TID_STATUS_NOT_OPERATIVE,  TID_STATUS_OPERATIVE;

    private TerminalIDstatus(){

    }
}
