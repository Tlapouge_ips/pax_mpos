package pax.ips.com.pax_mpos;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pax.ips.com.pax_mpos.imgprocessing.TesterImgProcessing;


public class ImgProcessingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.img_activity);
        Button bn_img = (Button) findViewById(R.id.bn_img);

        bn_img.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new TesterImgProcessing(ImgProcessingActivity.this).run();
            }
        });

    }
}
