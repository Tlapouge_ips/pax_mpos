package pax.ips.com.pax_mpos.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.pax.dal.IDAL;
import com.pax.neptunelite.api.NeptuneLiteUser;

import pax.ips.com.pax_mpos.activity.DALTestActivity;

public class GetObj {
    Context context;
    static IDAL dal = null;

    public GetObj(Context context) {

        this.context = context;

    }


    public IDAL ID() {


        try {
            dal = NeptuneLiteUser.getInstance().getDal(this.context);
        } catch (Exception e) {

        }
        return dal;
    }


    public static String logStr = "";

    // 获取IDal dal对象
    public static IDAL getDal() {

       // dal = sc.IdalGet();

        //if (dal == null) {
          //  Log.e("NeptuneLiteDemo", "dal is null");
        //}
        return dal;
    }

}
