package pax.ips.com.pax_mpos;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.pax.dal.IChannel;
import com.pax.dal.IDalCommManager;
import com.pax.dal.entity.EChannelType;
import com.pax.dal.exceptions.ChannelException;
import com.pax.dal.IDAL;
import com.pax.dal.exceptions.EChannelException;
import com.pax.neptunelite.api.NeptuneLiteUser;

import java.util.logging.Level;
import java.util.logging.Logger;

     class Communication{


    IDalCommManager commManager;
    IChannel channel;
    Logger logger = Logger.getLogger("myLogger");




         public Communication(com.pax.dal.IDAL dal)
    {
        commManager = dal.getCommManager();
    }

    public void EnableChannel(EChannelType channelType, int timeout)
    {
        logger.log(Level.SEVERE, "Comm.EnableChannel");

        try
        {


            commManager.enableChannelExclusive(channelType, timeout);
            if(commManager.enableMultiPath())
            {
                logger.log(Level.INFO, "Multi path enabled");
            }
        }
        catch(Exception e)
        {
            logger.log(Level.SEVERE, e.getMessage());
//            throw new EAException(EAException.EAErrorCode.FailedToConnect.ToString(), e);
        }
    }

    public void EnableChannel(EChannelType channelType)throws ChannelException
    {
        try
        {
            channel = commManager.getChannel(channelType);
            if(channel != null)
            {

                if (channel.isEnabled())
                {
                    logger.log(Level.SEVERE, "Comm Channel enabled");
                }
                else
                {
                    logger.log(Level.SEVERE, "Comm Channel not enabled");

                    channel.enable();
                }
            }
        }
        catch(ChannelException ce)
        {
            logger.log(Level.SEVERE, "Comm Channel exception: " + ce.getMessage());
            throw new ChannelException(EChannelException.DEVICES_ERR_UNEXPECTED);
        }
        catch(Exception e)
        {
            logger.log(Level.SEVERE, "Exception: " + e.getMessage());
            throw new ChannelException(EChannelException.DEVICES_ERR_NO_SUPPORT);
        }
    }

    public void DisableChannel(EChannelType channelType) throws ChannelException
    {
        try
        {
            if(channel != null && channel.isEnabled())
            {
                channel = null;
                logger.log(Level.SEVERE, "Comm Channel disabled");
            }
        }
        catch(Exception e)
        {

            logger.log(Level.SEVERE, "Comm Channel exception: " + e.getMessage());
            throw new ChannelException(EChannelException.DEVICES_ERR_CONNECT);

        }
    }

    public NetworkStatus GetNetworkStatus(EChannelType channelType)
    {
        if (channel == null)
        {
            channel = commManager.getChannel(channelType);
        }

        if(channel != null)
        {
            if(channel.isEnabled())
            {
                logger.log(Level.INFO, "Communication channel open");
                return NetworkStatus.OPEN;
            }
            else
            {
                logger.log(Level.INFO, "Communication channel closed");
                return NetworkStatus.CLOSED;
            }
        }

        logger.log(Level.INFO, "Communication channel error");
        return NetworkStatus.ERROR;
    }


    public enum NetworkStatus
    {
        OPEN,
        CLOSED,
        ERROR
    }
}
