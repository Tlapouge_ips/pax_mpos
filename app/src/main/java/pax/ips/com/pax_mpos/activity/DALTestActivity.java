package pax.ips.com.pax_mpos.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.pax.dal.IDAL;

import com.pax.neptunelite.api.NeptuneLiteUser;

import pax.ips.com.pax_mpos.R;
import pax.ips.com.pax_mpos.util.FloatView;
import pax.ips.com.pax_mpos.util.GetObj;

public class DALTestActivity extends Activity {

    private ListView listView;

    private List<String> nameList = null;
    public static IDAL idal = null;
    public FloatView floatView;

    public IDAL IdalGet() {


        try {

            idal = NeptuneLiteUser.getInstance().getDal(getApplicationContext());

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null == idal) {

            Toast.makeText(DALTestActivity.this, "error occurred,DAL is null.", Toast.LENGTH_LONG).show();

        }

        return idal;


    }

    @Override
    protected void onResume() {
        setScreen();
        GetObj.logStr = "";
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        floatView.removeFloatView();
        super.onDestroy();
    }

    private void fragmentSelect(Fragment fragment) {
        FragmentManager fManager = getFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        transaction.replace(R.id.parent_layout, fragment);
        transaction.commit();
    }

    private void setScreen() {
        int width = getWindowManager().getDefaultDisplay().getWidth();
        int height = getWindowManager().getDefaultDisplay().getHeight();
        if (width > height) {
            // do nothing
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }
}
