package pax.ips.com.pax_mpos.page;

import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import com.pax.dal.IDAL;
import com.pax.dal.IPrinter;

import pax.ips.com.pax_mpos.ATester;
import pax.ips.com.pax_mpos.MainActivity;
import pax.ips.com.pax_mpos.PageComposingActivity;
import pax.ips.com.pax_mpos.R;

import com.pax.dal.exceptions.PrinterDevException;
import com.pax.gl.page.IPage;

import com.pax.gl.page.PaxGLPage;
import com.pax.neptunelite.api.NeptuneLiteUser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

import static android.content.ContentValues.TAG;
import static java.lang.System.out;

public class TesterPageComposing extends ATester {
    Context context;
    public PaxGLPage iPaxGLPage;
    private Bitmap bitmap;

    private static final int FONT_BIG = 28;
    private static final int FONT_NORMAL = 24;
    private static final int FONT_BIGEST = 40;

    public TesterPageComposing(Context context) {
        this.context = context;
    }


    public void run() {

        iPaxGLPage = PaxGLPage.getInstance(context);
        IPage page = iPaxGLPage.createPage();
        String received = MainActivity.receiptView.getText().toString();
        String receivedd = MainActivity.result.getText().toString();

        if (received != "") {
            page.setTypeFace("/cache/data/public/neptune/Fangsong.ttf");
            // page.setTypefaceObj(Typeface.createFromAsset(context.getAssets(), "Fangsong.ttf"));
            IPage.ILine.IUnit unit = page.createUnit();
            unit.setAlign(IPage.EAlign.CENTER);
            unit.setText("TRANSACTION RESULTS");
            page.addLine().addUnit("ALTAPAY RECEIPT:", FONT_BIG, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit(received, FONT_NORMAL, IPage.EAlign.LEFT, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit("\n", FONT_NORMAL);
            page.addLine().addUnit("EXTRA FIELDS:", FONT_BIG, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit("\n", FONT_NORMAL);
            page.addLine().addUnit(receivedd, FONT_NORMAL, IPage.EAlign.LEFT, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit(getImageFromAssetsFile("pt.bmp"));


            page.addLine().addUnit("\n\n\n\n", FONT_BIG);



        }
        int width = 384;
        Bitmap bitmap = iPaxGLPage.pageToBitmap(page, width);

        setBitmap(bitmap);
        storeImage(bitmap);

    }



    private void storeImage(Bitmap image) {

        File sdcard = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/Screenshots/");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHss")
                .format(System.currentTimeMillis());


        File folder = new File(sdcard.getAbsoluteFile(), "PAX_MPOS_RESULT");
        if (!folder.exists())
            folder.mkdir();
        File file = new File(folder.getAbsoluteFile(), timeStamp + ".jpg");
        if (file.exists())
            file.delete();

        try {
            FileOutputStream out = new FileOutputStream(file);

            bitmap = Bitmap.createScaledBitmap(bitmap, 400, (int) (bitmap.getHeight() * (400.0 / bitmap.getWidth())), false);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Bitmap getImageFromAssetsFile(String fileName) {
        Bitmap image = null;
        AssetManager am = context.getResources().getAssets();
        try {
            InputStream is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;

    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;

    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }


    public void printBitmap(Bitmap b) {
        boolean result = false;
        if (b == null) {
            return;
        }

        IDAL dal = null;
        try {
            dal = NeptuneLiteUser.getInstance().getDal(this.context);
        } catch (Exception e) {

        }

        final IPrinter printer = dal.getPrinter();

        try {
            printer.init();
            printer.print(b, new IPrinter.IPinterListener() {
                @Override
                public void onSucc() {
                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (PrinterDevException e) {

        }

        return;

    }


}
