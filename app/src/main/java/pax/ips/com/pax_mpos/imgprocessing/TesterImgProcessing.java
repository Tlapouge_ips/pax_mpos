package pax.ips.com.pax_mpos.imgprocessing;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.pax.gl.impl.ImgProcessing;
import pax.ips.com.pax_mpos.ATester;

public class TesterImgProcessing extends ATester {
    Context context;

    public TesterImgProcessing(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        try {
            ImgProcessing ip = ImgProcessing.getInstance(context);
            Bitmap bitmap = BitmapFactory.decodeStream(context.getAssets().open("testImg/max.jpg"));

            byte[] jbg = ip.bitmapToJbig(bitmap, null);
            // byte[] jbg = ip.bitmapToJbig(bitmap, new IRgbToMonoAlgorithm() {
            //
            // @Override
            // public int evaluate(int r, int g, int b) {
            // return (r + g + b) / 3; // just test
            // // return 0; // all black
            // // return 1; // all white
            // // return new Random().nextInt(); // random dots
            // }
            // });

            Bitmap bitmapFromJbg = ip.jbigToBitmap(jbg);
            ImageView iv = new ImageView(context);
            iv.setImageBitmap(bitmapFromJbg);

            Dialog dialog = new Dialog(context);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(iv);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
