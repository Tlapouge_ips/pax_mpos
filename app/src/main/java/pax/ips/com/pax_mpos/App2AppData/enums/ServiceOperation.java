package pax.ips.com.pax_mpos.App2AppData.enums;

public enum ServiceOperation {
    SELECT_TID,  CLOSE_SESSION,  NOT_DEFINED, ONLINE_TOTALS;

    private ServiceOperation(){


    }
}
