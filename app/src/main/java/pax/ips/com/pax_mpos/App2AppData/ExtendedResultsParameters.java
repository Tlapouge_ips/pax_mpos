package pax.ips.com.pax_mpos.App2AppData;


public class ExtendedResultsParameters {
    public static final String RECEIPT = "receipt";
    public static final String STAN = "stan";
    public static final String ONLINE_ID = "onlineID";
    public static final String ACTION_CODE = "actionCode";
    public static final String MASKED_PAN = "maskedPan";
    public static final String CARD_INPUT_TYPE = "cardInputType";
    public static final String CARD_TYPE = "cardType";
    public static final String AUTHORIZATION_CODE = "authorizationCode";
    public static final String HOST_TIME_STAMP = "hostTimeStamp";
    public static final String ACQUIRER_ID = "acquirerID";
    public static final String ACQUIRER_NAME = "acquirerName";
    public static final String APPLICATION_ID = "applicationID";
    public static final String APPLICATION_LABEL = "applicationLabel";
    public static final String AMOUNT = "amount";
    public static final String DCC_EXCHANGE_RATE = "dccExchangeRate";
    public static final String DCC_CURRENCY_CODE = "dccCurrencyCode";
    public static final String DCC_AMOUNT = "dccAmount";
    public static final String DCC_AMOUNT_PRECISION = "dccPrecision";
    public static final String CUSTOM_TLV_DATA = "additionalCustomData";
    public static final String LOCAL_TOTALS = "localSessionTotals";
    public static final String HOST_TOTALS = "hostSessionTotals";
    public static final String FIELD62DATA ="field62Data";

    public ExtendedResultsParameters() {
    }
}