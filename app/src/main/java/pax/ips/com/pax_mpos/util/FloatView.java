package pax.ips.com.pax_mpos.util;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.TextView;

import pax.ips.com.pax_mpos.R;


public class FloatView implements OnLongClickListener {
    private Context c;

    public FloatView(Context c) {
        this.c = c;
    }

    private WindowManager wm;
    private TextView view;// 浮动按钮
    private long startTime = 0;

    /**
     * 创建悬浮视图
     * 
     * @param paddingTop
     *            初始上边距
     * @param paddingRight
     *            初始右边距
     */
    public void createFloatView(int paddingTop, int paddingRight) {
        // int w = 80;// 大小
        wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        view = (TextView) LayoutInflater.from(c).inflate(R.layout.floatview, null);
        view.setMovementMethod(ScrollingMovementMethod.getInstance());
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.type = WindowManager.LayoutParams.TYPE_BASE_APPLICATION;// 所有程序窗口的“基地”窗口，其他应用程序窗口都显示在它上面。
        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        params.format = PixelFormat.TRANSLUCENT;// 不设置这个弹出框的透明遮罩显示为黑色
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.TOP | Gravity.LEFT;
        int screenWidth = c.getResources().getDisplayMetrics().widthPixels;
        // int screenHeight = c.getResources().getDisplayMetrics().heightPixels;
        params.x = screenWidth - paddingRight - 50;
        params.y = paddingTop;
        view.setBackgroundColor(Color.GRAY);
        view.setVisibility(View.VISIBLE);
        view.setOnLongClickListener(this);
        view.setOnTouchListener(new View.OnTouchListener() {
            // 触屏监听
            float lastX, lastY;
            int oldOffsetX, oldOffsetY;
            int tag = 0;// 悬浮球 所需成员变量

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int action = event.getAction();
                float x = event.getX();
                float y = event.getY();
                if (tag == 0) {
                    oldOffsetX = params.x; // 偏移量
                    oldOffsetY = params.y; // 偏移量
                }
                if (action == MotionEvent.ACTION_DOWN) {
                    lastX = x;
                    lastY = y;
                } else if (action == MotionEvent.ACTION_MOVE) {
                    params.x += (int) (x - lastX) / 3; // 减小偏移量,防止过度抖动
                    params.y += (int) (y - lastY) / 3; // 减小偏移量,防止过度抖动
                    tag = 1;
                    wm.updateViewLayout(view, params);
                } else if (action == MotionEvent.ACTION_UP) {
                    int newOffsetX = params.x;
                    int newOffsetY = params.y;
                    // 只要按钮移动位置不是很大,就认为是点击事件
                    if (Math.abs(oldOffsetX - newOffsetX) <= 20 && Math.abs(oldOffsetY - newOffsetY) <= 20) {
                        onFloatViewClick(l);
                    } else {
                        tag = 0;
                    }

                    if ((System.currentTimeMillis() - startTime) < 500) {
                        // doubleClick
                        view.setBackgroundColor(Color.GRAY);
                        view.setText("Log");
                    } else {
                        startTime = System.currentTimeMillis();
                    }
                }
                return false;
            }
        });
        wm.addView(view, params);
    }

    /**
     * 点击浮动按钮触发事件，需要override该方法
     */
    private View.OnClickListener l;

    public void onFloatViewClick(View.OnClickListener l) {
        this.l = l;
        // Toast.makeText(c, "FLOAT", Toast.LENGTH_SHORT).show();
        // if (GetObj.logStr != null && GetObj.logStr.length() > 0) {
        // if (GetObj.logStr.substring(0, 1).equals("t")) {
        // view.setBackgroundColor(Color.GREEN);
        // } else if(GetObj.logStr.substring(0, 1).equals("e")) {
        // view.setBackgroundColor(Color.RED);
        // }else{
        // view.setBackgroundColor(Color.GRAY);
        // }
        // }
        view.setText(GetObj.logStr.equals("") ? "Log" : GetObj.logStr);
        view.setOnClickListener(l);
    }

    /**
     * 将悬浮View从WindowManager中移除，需要与createFloatView()成对出现
     */
    public void removeFloatView() {
        if (wm != null && view != null) {
            wm.removeViewImmediate(view);
            // wm.removeView(view);//不要调用这个，WindowLeaked
            view = null;
            wm = null;
        }
    }

    /**
     * 隐藏悬浮View
     */
    public void hideFloatView() {
        if (wm != null && view != null && view.isShown()) {
            view.setVisibility(View.GONE);
        }
    }

    /**
     * 显示悬浮View
     */
    public void showFloatView() {
        if (wm != null && view != null && !view.isShown()) {
            view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        Builder builder = new Builder(c);
        builder.setTitle("Delete log?");
        builder.setNegativeButton("Cancel", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Yes", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                GetObj.logStr = "";
                view.setBackgroundColor(Color.GRAY);
                view.setText("Log");
                view.invalidate();
            }
        });
        builder.create().show();
        return true;
    }

}
